(function ($) {
    'use strict';
    $.fn.zeditor = function() {
        var editor = this,
            selectedRange,
            options,
            execCommand = function(commandWithArgs, valueArg) {
                var commandArr = commandWithArgs.split(' '),
                    command = commandArr.shift(),
                    args = commandArr.join(' ') + (valueArg || '');
                document.execCommand(command, 0 , args);
            },
            bindToolbar = function(toolbar, options) {
                toolbar.find(options.toolbarBtnSelector).click(function() {
                    execCommand($(this).data('edit'));
                });
            },
            initFontBindings = function(fontsArr) {
                var fonts = fontsArr,
                    fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                $.each(fonts, function (idx, fontName) { 
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName +
                        '" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
                  });
            };

        options = $.extend({}, $.fn.zeditor.defaults);
        editor.attr('contenteditable', true);
        initFontBindings(options.fontsArr);
        bindToolbar($(options.toolbarSelector), options);

        return this;
    };

    $.fn.zeditor.defaults  = {
        toolbarSelector: '[data-role=editor-toolbar]',
        toolbarBtnSelector : 'a[data-edit],button[data-edit],input[type=button][data-edit]',
        fontsArr: ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
            'Times New Roman', 'Verdana']
    };
}(window.jQuery));
